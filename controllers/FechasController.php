<?php

namespace app\controllers;

use app\models\Fechas;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * FechasController implements the CRUD actions for Fechas model.
 */
class FechasController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all Fechas models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Fechas::find(),
            /*
            'pagination' => [
                'pageSize' => 50
            ],
            'sort' => [
                'defaultOrder' => [
                    'idFechas' => SORT_DESC,
                ]
            ],
            */
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Fechas model.
     * @param int $idFechas Id Fechas
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($idFechas)
    {
        return $this->render('view', [
            'model' => $this->findModel($idFechas),
        ]);
    }

    /**
     * Creates a new Fechas model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new Fechas();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'idFechas' => $model->idFechas]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Fechas model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $idFechas Id Fechas
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($idFechas)
    {
        $model = $this->findModel($idFechas);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'idFechas' => $model->idFechas]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Fechas model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $idFechas Id Fechas
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($idFechas)
    {
        $this->findModel($idFechas)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Fechas model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $idFechas Id Fechas
     * @return Fechas the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($idFechas)
    {
        if (($model = Fechas::findOne(['idFechas' => $idFechas])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
