<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Llevan */

$this->title = 'Update Llevan: ' . $model->codLlevan;
$this->params['breadcrumbs'][] = ['label' => 'Llevans', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->codLlevan, 'url' => ['view', 'codLlevan' => $model->codLlevan]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="llevan-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
