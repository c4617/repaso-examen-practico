<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Llevan */

$this->title = $model->codLlevan;
$this->params['breadcrumbs'][] = ['label' => 'Llevans', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="llevan-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'codLlevan' => $model->codLlevan], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'codLlevan' => $model->codLlevan], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'codLlevan',
            'codCamioneros',
            'codCamiones',
        ],
    ]) ?>

</div>
