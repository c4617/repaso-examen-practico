<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Llevan */

$this->title = 'Create Llevan';
$this->params['breadcrumbs'][] = ['label' => 'Llevans', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="llevan-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
