<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Paquetes */

$this->title = $model->codPaquetes;
$this->params['breadcrumbs'][] = ['label' => 'Paquetes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="paquetes-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'codPaquetes' => $model->codPaquetes], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'codPaquetes' => $model->codPaquetes], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'codPaquetes',
            'descripcion',
            'direccionDes',
            'codCamioneros',
            'codProvincias',
        ],
    ]) ?>

</div>
