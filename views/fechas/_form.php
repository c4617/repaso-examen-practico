<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Fechas */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="fechas-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'codCamioneros')->textInput() ?>

    <?= $form->field($model, 'codCamiones')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
