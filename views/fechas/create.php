<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Fechas */

$this->title = 'Create Fechas';
$this->params['breadcrumbs'][] = ['label' => 'Fechas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="fechas-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
