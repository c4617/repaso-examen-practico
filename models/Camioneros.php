<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "camioneros".
 *
 * @property int $codCamioneros
 * @property string $nombre
 * @property string $poblacion
 * @property string $telefono
 * @property int $direccion
 * @property int|null $salario
 *
 * @property Fechas[] $fechas
 * @property Llevan[] $llevans
 * @property Paquetes[] $paquetes
 */
class Camioneros extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'camioneros';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre', 'poblacion', 'telefono', 'direccion'], 'required'],
            [['direccion', 'salario'], 'integer'],
            [['nombre', 'poblacion', 'telefono'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codCamioneros' => 'Cod Camioneros',
            'nombre' => 'Nombre',
            'poblacion' => 'Poblacion',
            'telefono' => 'Telefono',
            'direccion' => 'Direccion',
            'salario' => 'Salario',
        ];
    }

    /**
     * Gets query for [[Fechas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getFechas()
    {
        return $this->hasMany(Fechas::className(), ['codCamioneros' => 'codCamioneros']);
    }

    /**
     * Gets query for [[Llevans]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getLlevans()
    {
        return $this->hasMany(Llevan::className(), ['codCamioneros' => 'codCamioneros']);
    }

    /**
     * Gets query for [[Paquetes]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPaquetes()
    {
        return $this->hasMany(Paquetes::className(), ['codCamioneros' => 'codCamioneros']);
    }
}
