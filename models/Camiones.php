<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "camiones".
 *
 * @property int $codCamiones
 * @property string $modelo
 * @property string $tipo
 * @property int $potencia
 *
 * @property Fechas[] $fechas
 * @property Llevan[] $llevans
 */
class Camiones extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'camiones';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['modelo', 'tipo', 'potencia'], 'required'],
            [['potencia'], 'integer'],
            [['modelo', 'tipo'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codCamiones' => 'Cod Camiones',
            'modelo' => 'Modelo',
            'tipo' => 'Tipo',
            'potencia' => 'Potencia',
        ];
    }

    /**
     * Gets query for [[Fechas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getFechas()
    {
        return $this->hasMany(Fechas::className(), ['codCamiones' => 'codCamiones']);
    }

    /**
     * Gets query for [[Llevans]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getLlevans()
    {
        return $this->hasMany(Llevan::className(), ['codCamiones' => 'codCamiones']);
    }
}
